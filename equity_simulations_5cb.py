import numpy as np
import itertools
import operator as op
from functools import reduce

def pair(comb):
    values_only = np.mod(comb,13)
    unique, counts = np.unique(values_only, return_counts=True)
    n = max(counts)
    bins = np.bincount(counts, minlength = 5)
    if n == 1:
        return nopair(unique)
    elif n == 2:
        if bins[2]==1:
            return single(unique, counts)
        else:
            return double(unique, counts)
    elif n == 3:
        if bins[2]==0 and np.bincount(counts)[3]==1:
            return triple(unique, counts)
        else:
            return full_house(unique, counts)
    elif n == 4:
        return quads(unique, counts)
    else:
        print ("deck  error")
        print (comb)
        print (values_only)
    return 0

def nopair(values_only):
    val = 0
    for i in range(5):
        val += values_only[i]*(13**i)
    return val

def single(unique, counts):
    p = np.where(counts == 2)
    val = unique[p[0][0]]*13**3
    unique = np.delete(unique, p[0][0])
    for i in range(3):
        val += unique[i]*(13**i)
    val+=10**6
    return val

def double(unique, counts):
    p = np.where(counts == 2)
    val = unique[p[0][-1]]*13**2
    val += unique[p[0][-2]]*13
    unique = np.delete(unique, [p[0][-1],p[0][-2]])
    val += unique[0]
    val+=2*10**6
    return val

def triple(unique, counts):
    p = np.where(counts == 3)
    val = unique[p[0][0]]*13**2
    unique = np.delete(unique, p[0][0])
    for i in range(2):
        val += unique[i]*(13**i)
    val += 3*10**6
    return val

def full_house(unique, counts):
    p = np.where(counts == 3)
    val = unique[p[0][-1]]*13
    unique = np.delete(unique, p[0][-1])
    counts = np.delete(counts, p[0][-1])
    val+=unique[0]
    val += 6*10**6
    return val

def quads(unique, counts):
    p = np.where(counts == 4)
    val = unique[p[0][0]]*13
    unique = np.delete(unique, p[0][0])
    val += unique[0]
    val += 7*10**6
    return val




def straight(comb):
    values_only = np.mod(comb,13)
    unique = np.unique(values_only)
    n = len(unique)
    if n == 5:
        if unique[4]-unique[0]==4:
            return (4*10**6+unique[4])
        elif unique[4]==12:
            if unique[3]==3:
                return (4*10**6+3)
    return 0


def flush(comb):
    suit = comb//13
    values_only = np.mod(comb,13)
    unique = np.unique(suit)
    n = len(unique)
    if n == 1:
        val = 0
        for i in range(5):
            val += values_only[i]*(13**i)
            return 5*10**6 + val
    return 0



def ranking(comb):
    str = straight(comb)
    flu = flush(comb)
    if str!=0 and flu!=0:
        return str+4*10**6
    else:
        pr = pair(comb)
        score = max(str,flu,pr)
    return score


def gen_combs():
    ranks = np.zeros(int(ncr(52,5)), dtype = np.int32)
    comb = np.zeros(5, dtype = np.int32)
    count = 0
    for i in range(48):
        comb[0] = i
        for j in range(i+1,49):
            comb[1] = j
            for k in range(j+1,50):
                comb[2] = k
                for l in range(k+1,51):
                    comb[3] = l
                    for m in range(l+1,52):
                        comb[4] = m
                        ranks[count] = ranking(comb)
                        count+=1
    print (count)
    return ranks


def comb_map(comb):#this function will map a given combination to its position in itertools.combinations(deck, r = 7)
    i = comb[0]
    j = comb[1]
    k = comb[2]
    l = comb[3]
    m = comb[4]
    n = comb[5]
    o = comb[6]
    val = 0
    for a in range(i):
        val+= ncr(52-a-1,6)
    for b in range(j-i-1):
        val+= ncr(52-b-i-2,5)
    for c in range(k-j-1):
        val+= ncr(52-c-j-2,4)
    for d in range(l-k-1):
        val+= ncr(52-d-k-2,3)
    for e in range(m-l-1):
        val+= ncr(52-e-l-2,2)
    for f in range(n-m-1):
        val+= 52-f-m-2#optimise this and others if possible
    val+=o-n-1

    return val

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer / denom

ranks = np.zeros(int(ncr(52,5)), dtype = np.int32)
ranks = gen_combs()
np.save('all_5_card_boards_ranked.npy', ranks)





"""
deck = np.arange(0,52)

cards = np.array([0,1,2,3])

p1=np.array([cards[0],cards[1]])
p2=np.array([cards[2],cards[3]])

deck = np.delete(deck,cards)
h1 = 0
h2 = 0
draw = 0
board = np.zeros(5, dtype = np.int32)
combs1 = np.zeros(5, dtype = np.int32)
combs2 = np.zeros(5, dtype = np.int32)
for i in range(44):
    board[0] = deck[i]
    for j in range(i+1,45):
        board[1] = deck[j]
        print (j)
        for k in range(j+1,46):
            board[2] = deck[k]
            for l in range(k+1,47):
                board[3] = deck[l]
                for m in range(l+1,48):
                    board[4] = deck[m]
                    b1 = np.sort(np.append(p1,board))
                    b2 = np.sort(np.append(p2,board))
                    r1=0
                    r2=0
                    for n in range(3):
                        combs1[0] = b1[n]
                        combs2[0] = b2[n]
                        for o in range(n+1,4):
                            combs1[1] = b1[o]
                            combs2[1] = b2[o]
                            for p in range(o+1,5):
                                combs1[2] = b1[p]
                                combs2[2] = b2[p]
                                for q in range(p+1,6):
                                    combs1[3] = b1[q]
                                    combs2[3] = b2[q]
                                    for r in range(q+1,7):
                                        combs1[4] = b1[r]
                                        combs2[4] = b2[r]
                                        r1 = max(r1,ranking(combs1))
                                        r2 = max(r2,ranking(combs2))


                    if r1>r2:
                        h1+=1
                    elif r1==r2:
                        draw+=1
                    else:
                        h2+=1

print(h1)
print(h2)
print(draw)

board = np.array([39,40,41,35,42])
board-=13
print (board)
print (p1)
print (p2)
print (ranking(p1,board))
print (ranking(p2,board))
"""
