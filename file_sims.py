import numpy as np
import itertools
import operator as op
from functools import reduce
from scipy.special import binom
import time

def comb_map(comb):#this function will map a given combination to its position in itertools.combinations(deck, r = 7)
    i = comb[0]
    j = comb[1]
    k = comb[2]
    l = comb[3]
    m = comb[4]
    n = comb[5]
    o = comb[6]
    val = tot_combs
    """
    for a in range(i):
        val+= ncr(52-a-1,6)
    for b in range(j-i-1):
        val+= ncr(52-b-i-2,5)
    for c in range(k-j-1):
        val+= ncr(52-c-j-2,4)
    for d in range(l-k-1):
        val+= ncr(52-d-k-2,3)
    for e in range(m-l-1):
        val+= ncr(52-e-l-2,2)
    for f in range(n-m-1):
        val+= 52-f-m-2#optimise this and others if possible
    """

    """
    val+=binom(52,7)-binom(52-i,7)
    val+=binom(51-i,6)-binom(52-j,6)
    val+=binom(51-j,5)-binom(52-k,5)
    val+=binom(51-k,4)-binom(52-l,4)
    val+=binom(51-l,3)-binom(52-m,3)
    val+=((102-(m+n))*(n-m-1))/2
    val+=o-n-1
    """
    val-=(binom(51-i,6)/7)*(45-i)
    val-=(binom(51-j,5)/6)*(46-j)
    val-=(binom(51-k,4)/5)*(47-k)
    val-=(binom(51-l,3)/4)*(48-l)
    val-=binom(52-m,3)
    val+=((102-(m+n))*(n-m-1))/2
    val+=o-n-1


    return int(val)
"""
def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer / denom
"""

def gen_combs():
    comb = np.zeros(7, dtype = np.int32)
    count = 0
    for i in range(52):
        comb[0] = i
        for j in range(i+1,52):
            comb[1] = j
            for k in range(j+1,52):
                comb[2] = k
                for l in range(k+1,52):
                    comb[3] = l
                    for m in range(l+1,52):
                        comb[4] = m
                        for n in range(m+1,52):
                            comb[5] = n
                            for o in range(n+1,52):
                                comb[6] = o
                                count+=1
    return count




ranks = np.load('all_7_card_boards_ranked.npy')

deck = np.arange(0,52)

cards = np.array([51,48,49,50])

p1=np.array([cards[0],cards[1]])
p2=np.array([cards[2],cards[3]])


deck = np.delete(deck,cards)

x = itertools.combinations(deck, r = 5)
combs = np.array(tuple(x))
n = len(combs)

tot_combs = 133784560
h1 = 0
h2 = 0
draw = 0
map_time = 0
acc_time = 0
for i in range(n):
    b1 = np.sort(np.append(p1,combs[i]))
    b2 = np.sort(np.append(p2,combs[i]))
    #start = time.time()
    """
    m1 = comb_map(b1)
    m2 = comb_map(b2)
    """
    m1 = 1000000
    m2 = 2000000
    #map_time+=time.time()-start
    #start = time.time()
    r1 = ranks[m1]
    r2 = ranks[m2]
    #acc_time+=time.time()-start
    if r1>r2:
        h1+=1
    elif r1==r2:
        draw+=1
    else:
        h2+=1
#print (map_time)
#print (acc_time)
print(h1)
print(h2)
print(draw)
"""
print (comb_map([0,1,2,3,6,10,11]))
print (binom(2,3))
"""
