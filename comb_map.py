import numpy as np
import itertools
import operator as op
from functools import reduce

def comb_map(comb):#this function will map a given combination to its position in itertools.combinations(deck, r = 7)
    i = comb[0]
    j = comb[1]
    k = comb[2]
    l = comb[3]
    m = comb[4]
    n = comb[5]
    o = comb[6]
    val = 0
    for a in range(i):
        val+= ncr(52-a-1,6)
    for b in range(j-i-1):
        val+= ncr(52-b-i-2,5)
    for c in range(k-j-1):
        val+= ncr(52-c-j-2,4)
    for d in range(l-k-1):
        val+= ncr(52-d-k-2,3)
    for e in range(m-l-1):
        val+= ncr(52-e-l-2,2)
    for f in range(n-m-1):
        val+= 52-f-m-2#optimise this and others if possible
    val+=o-n-1

    return val
"""
def comb_map5(comb):#this function will map a given combination to its position in itertools.combinations(deck, r = 7)
    i = comb[0]
    j = comb[1]
    k = comb[2]
    l = comb[3]
    m = comb[4]
    val = 0
    for a in range(i):
        val+= ncr(52-a-1,4)
        print (ncr(52-a-1,4))
    for b in range(j-i-1):
        val+= ncr(52-b-2-i,3)
        print (ncr(52-b-2-i,3))
    for c in range(k-j-1):
        val+= ncr(52-c-j-2,2)
        print (ncr(52-c-j-2,2))
    for d in range(l-k-1):
        val+= 52-d-k-2
        print (52-d-k-4)
    val+=m-l-1
    print (m-l-1)

    return val


def comb_map3(comb,n):#this function will map a given combination to its position in itertools.combinations(deck, r = 7)
    i = comb[0]
    j = comb[1]
    k = comb[2]

    val = 0
    for a in range(i):
        val+= ncr(n-a-1,2)
        print (ncr(n-a-1,2))
    for b in range(j-i-1):
        val+= n-b-2-i
        print (n-b-2-i)
    val+=k-j-1
    print (k-j-1)

    return val
"""

def gen_combs():
    comb = np.zeros(7, dtype = np.int32)
    count = 0
    for i in range(52):
        comb[0] = i
        for j in range(i+1,52):
            comb[1] = j
            for k in range(j+1,52):
                comb[2] = k
                for l in range(k+1,52):
                    comb[3] = l
                    for m in range(l+1,52):
                        comb[4] = m
                        for n in range(m+1,52):
                            comb[5] = n
                            for o in range(n+1,52):
                                comb[6] = o
                                count+=1
    return count

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer / denom


"""
deck = np.arange(0,52)
x = itertools.combinations(deck, r = 5)
combs = np.array(tuple(x))
k = 1002379
#print (combs[k-1])
#print (combs[k])
print (comb_map(combs[k]))

x = itertools.combinations(np.arange(6), r = 3)
combs = np.array(tuple(x))
k = 20
print (combs[k-1])
print (combs[k])
print (comb_map2(combs[k],6))
"""
print (gen_combs())
