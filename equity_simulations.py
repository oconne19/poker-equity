import numpy as np
import itertools
import operator as op
from functools import reduce

def pair(comb):
    values_only = np.mod(comb,13)
    unique, counts = np.unique(values_only, return_counts=True)
    n = max(counts)
    bins = np.bincount(counts, minlength = 5)
    if n == 1:
        return nopair(unique)
    elif n == 2:
        if bins[2]==1:
            return single(unique, counts)
        else:
            return double(unique, counts)
    elif n == 3:
        if bins[2]==0 and np.bincount(counts)[3]==1:
            return triple(unique, counts)
        else:
            return full_house(unique, counts)
    elif n == 4:
        return quads(unique, counts)
    else:
        print ("deck  error")
    return 0

def nopair(values_only):
    val = 0
    for i in range(5):
        val += values_only[6-i]*(13**(4-i))
    return val

def single(unique, counts):
    p = np.where(counts == 2)
    val = unique[p[0][0]]*13**3
    unique = np.delete(unique, p[0][0])
    for i in range(3):
        val += unique[4-i]*13**(2-i)
    val+=10**6
    return val

def double(unique, counts):
    p = np.where(counts == 2)
    val = unique[p[0][-1]]*13**2
    val += unique[p[0][-2]]*13
    unique = np.delete(unique, [p[0][-1],p[0][-2]])
    val += unique[-1]
    val+=2*10**6
    return val

def triple(unique, counts):
    p = np.where(counts == 3)
    val = unique[p[0][0]]*13**2
    unique = np.delete(unique, p[0][0])
    for i in range(2):
        val += unique[3-i]*13**(1-i)
    val += 3*10**6
    return val

def full_house(unique, counts):
    p = np.where(counts == 3)
    val = unique[p[0][-1]]*13
    unique = np.delete(unique, p[0][-1])
    counts = np.delete(counts, p[0][-1])
    j=1
    i=1
    while j:
        if counts[-i]>1:
            val += unique[-i]
            j=0
        if i == (len(unique)+1):
            print ("full_house error")
        i += 1
    val += 6*10**6
    return val

def quads(unique, counts):
    p = np.where(counts == 4)
    val = unique[p[0][0]]*13
    unique = np.delete(unique, p[0][0])
    val += unique[-1]
    val += 7*10**6
    return val




def straight(comb):
    values_only = np.mod(comb,13)
    unique, counts = np.unique(values_only, return_counts=True)
    n = len(unique)
    prev = unique[-1]
    count = 1
    for i in range(n):
        if (prev-unique[n-2-i])==1:
            count +=1
        else:
            count = 1
        if count == 5:
            return (4*10**6+unique[n-i+2])
        prev = unique[n-2-i]
    if unique[-1] == 12:
        if n >4 and np.array_equal(np.array([unique[0],unique[1],unique[2],unique[3]]),np.array([0,1,2,3])):
            return (4*10**6+3)
    return 0


def flush(comb):
    count = 1
    current = comb[0]//13
    for i in range(1,7):
        if comb[i]//13==current:
            count+=1
        elif count>=5:
            val = 0
            for j in range(5):
                val += np.mod(comb[i-j-1],13)*(13**(4-j))
            return 5*10**6 + val
        elif count<5:
            count = 1
            current = comb[i]//13
    if count>=5:
        val = 0
        for j in range(5):
            val += np.mod(comb[i-j],13)*(13**(4-j))
        return 5*10**6 + val

    return 0

def straight_flush(comb):
    count = 1
    holder = 0
    current = comb[0]//13
    for i in range(1,7):
        if comb[i]//13==current:
            if (comb[i]-comb[i-1])==1:
                count+=1
            elif count>=5:
                return 8*10**6 + np.mod(comb[i-1],13)
            else:
                count = 1

        elif count>=5:
            return 8*10**6 + np.mod(comb[i-1],13)
        elif np.mod(comb[i-1],13)==12 and i-holder>=4:
            if np.array_equal(np.mod([comb[holder],comb[holder+1],comb[holder+2],comb[holder+3]],13),np.array([0,1,2,3])):
                return 8*10**6 + 3
        elif count<5:
            count = 1
            holder = i
            current = comb[i]//13
        #print (count)
    if count>=5:
        return 8*10**6 + np.mod(comb[i],13)
    if np.mod(comb[i],13)==12 and i-holder>=4:
        if np.array_equal(np.mod([comb[holder],comb[holder+1],comb[holder+2],comb[holder+3]],13),np.array([0,1,2,3])):
            return 8*10**6 + 3

    return 0

def ranking(comb):
    score = pair(comb)
    if score>=6*10**6:
        score = max(straight_flush(comb),score)
        return score
    else:
        str = straight(comb)
        flu = flush(comb)
        score = max(str,flu,score)
        if str!=0 and flu!=0:
            #print ('yo')
            strflu = straight_flush(comb)
            #print(strflu)
            score = max(score,strflu)
    return score


def gen_combs():
    ranks = np.zeros(int(ncr(52,7)), dtype = np.int32)
    comb = np.zeros(7, dtype = np.int32)
    count = 0
    for i in range(52):
        comb[0] = i
        for j in range(i+1,52):
            comb[1] = j
            for k in range(j+1,52):
                comb[2] = k
                for l in range(k+1,52):
                    comb[3] = l
                    for m in range(l+1,52):
                        comb[4] = m
                        for n in range(m+1,52):
                            comb[5] = n
                            for o in range(n+1,52):
                                comb[6] = o
                                ranks[count] = ranking(comb)
                                count+=1
    return ranks


def comb_map(comb):#this function will map a given combination to its position in itertools.combinations(deck, r = 7)
    i = comb[0]
    j = comb[1]
    k = comb[2]
    l = comb[3]
    m = comb[4]
    n = comb[5]
    o = comb[6]
    val = 0
    for a in range(i):
        val+= ncr(52-a-1,6)
    for b in range(j-i-1):
        val+= ncr(52-b-i-2,5)
    for c in range(k-j-1):
        val+= ncr(52-c-j-2,4)
    for d in range(l-k-1):
        val+= ncr(52-d-k-2,3)
    for e in range(m-l-1):
        val+= ncr(52-e-l-2,2)
    for f in range(n-m-1):
        val+= 52-f-m-2#optimise this and others if possible
    val+=o-n-1

    return val

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer / denom


ranks = gen_combs()
np.save('all_7_card_boards_ranked.npy', ranks)





"""
deck = np.arange(0,52)

cards = np.array([2,6,24,8])

p1=np.array([cards[0],cards[1]])
p2=np.array([cards[2],cards[3]])


deck = np.delete(deck,cards)


x = itertools.combinations(deck, r = 5)
combs = np.array(tuple(x))
n = len(combs)
h1 = 0
h2 = 0
draw = 0
for i in range(n):
    board = combs[i]
    r1 = ranking(p1,board)
    r2 = ranking(p2,board)
    if r1>r2:
        h1+=1
    elif r1==r2:
        draw+=1
    else:
        h2+=1
print(h1)
print(h2)
print(draw)

board = np.array([39,40,41,35,42])
board-=13
print (board)
print (p1)
print (p2)
print (ranking(p1,board))
print (ranking(p2,board))
"""
